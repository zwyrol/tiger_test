<?php

namespace Tests\AppBundle;

use AppBundle\Components\Feed;
use AppBundle\Components\FeedResult;
use PHPUnit\Framework\TestCase;

class FeedTest extends TestCase
{
    protected function setUp()
    {
    }

    public function testFeedResult() {
        $tags = ['microwave', 'oven'];
        $url = 'http://craft.com/products/3';
        $title = 'Microwave XX 50';

        $feedResult = new FeedResult();
        $feedResult->setTags($tags);
        $feedResult->setUrl($url);
        $feedResult->setTitle($title);

        $this->assertEquals($feedResult->getTags(), $tags);
        $this->assertEquals($feedResult->getUrl(), $url);
        $this->assertEquals($feedResult->getTitle(), $title);
    }

    public function testGetFeedInstance()
    {
        $correctInstances = ['craft', 'gift'];

        foreach($correctInstances as $instanceName) {
            $feed = Feed::getInstance($instanceName);

            $this->assertInstanceOf(Feed::class, $feed);
        }

        $invalidInstances = ['blah', 'foo', 'banana'];

        foreach($invalidInstances as $instanceName) {
            $this->expectException(\Exception::class);

            Feed::getInstance($instanceName);
        }
    }
}