<?php

namespace AppBundle\Command;

use AppBundle\Components\Feed;
use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('import')
            ->setDefinition(array(
                                new InputArgument('source', InputArgument::REQUIRED, 'The Feed Source'),
                            ))
            ->setDescription('Import Feeds Command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = $input->getArgument('source');
        $feed = Feed::getInstance($source);
        $feed->import();
    }
}