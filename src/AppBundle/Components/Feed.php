<?php

namespace AppBundle\Components;

class Feed
{
    const GIFT  = 'gift';
    const CRAFT = 'craft';

    /**
     * @var array
     */
    static $allowedSources = [self::GIFT, self::CRAFT];

    /**
     * @return void
     */
    public function import() {
        $data = $this->readData();

        foreach($data as $feedResult) {
            echo 'Importing: ' . $feedResult->getTitle() . ' url: ' . $feedResult->getUrl() . ' tags: ' . join(', ', $feedResult->getTags()) . "\n";
        }
    }

    /**
     * @param $sourceName
     *
     * @return mixed
     * @throws \Exception
     */
    public static function getInstance($sourceName)
    {
        if (!in_array($sourceName, self::$allowedSources)) {
            throw new \Exception("The source: {$sourceName} doesn't exists");
        }

        $className = '\\AppBundle\Components\\' . ucfirst(($sourceName)) . 'Feed';

        if (!class_exists($className)) {
            throw new \Exception("The Feed class: {$className} doesn't exists.");
        }

        $feed = new $className;

        return $feed;
    }
}