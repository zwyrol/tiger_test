<?php

namespace AppBundle\Components;

class GiftFeed extends Feed
{
    /**
     * @var string
     */
    private $dataYaml = '
---
- labels:
  - chocolate
  - swiss
  name: toblerone
  url: http://gift.com/catalogue/41636
- labels:
  - disney
  - clothing
  name: mens mickey mouse t-shirt
  url: http://gift.com/catalogue/c4618
- labels:
  - clean
  - basics
  name: basic broom
  url: http://gift.com/catalogue/d5045
- labels:
  - clean
  - basics
  name: die hard on dvd
  url: http://gift.com/catalogue/cd3d9
...
';

    /**
     * @return array
     */
    public function readData() {
        $data = yaml_parse($this->dataYaml);

        $ret = [];

        foreach($data as $product) {
            $feedResult = new FeedResult();

            if(isset($product['labels'])) {
                $feedResult->setTags($product['labels']);
            }

            $feedResult->setUrl($product['url']);
            $feedResult->setTitle($product['name']);

            $ret[] = $feedResult;
        }

        return $ret;
    }
}