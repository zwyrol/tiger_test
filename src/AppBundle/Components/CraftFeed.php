<?php

namespace AppBundle\Components;

class CraftFeed extends Feed
{
    /**
     * @var string
     */
    private $dataJson = '
{
	"products": [{
			"tags": [
				"microwave",
				"oven"
			],
			"url": "http://craft.com/products/3",
			"title": "microwave XX 20"
		},
		{
			"tags": [
				"perfume",
				"makeup"
			],
			"url": "http://craft.com/products/4",
			"title": "makeup revolution by love"
		}
	]
}';

    /**
     * @return array
     */
    public function readData() {
        $data = json_decode($this->dataJson);

        $ret = [];

        foreach($data->products as $product) {
            $feedResult = new FeedResult();

            if(isset($product->tags)) {
                $feedResult->setTags($product->tags);
            }

            $feedResult->setUrl($product->url);
            $feedResult->setTitle($product->title);

            $ret[] = $feedResult;
        }

        return $ret;
    }
}