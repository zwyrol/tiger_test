1. Installation

Please run following commands:

git clone git@bitbucket.org:zwyrol/tiger_test.git
cd tiger_spike/docker
sh build.sh && sh run.sh

It will login you to docker container and you will be able to test application.

2. Testing application

Once you get Docker Bash You can run unit tests by:

[UNIT TEST COMMAND]

and then test application itself:

./bin/console import gift
./bin/console import craft

3. My Code can be found in following namespaces: 
    - src/AppBundle/Components
    - src/AppBundle/Command
    - tests/AppBundle
    
4. Was it your first time writing a unit test, using a particular framework, etc?
No - I've did them in past too

5. What would you have done differently if you had had more time

a. Add Better Exception Names if instance not found or class not found
b. I've hard coded the json and yaml in to classes instead of creating separated files 
c. Build the service/gateway for that and then we will be able to reuse also in http controllers
d. Think about generators if the feed source is hudge
e. Re-write readData functions and write unit tests for it
f. Write test for import function if it will persist data somehow
g. Link FeedResults with DataObjects and display them in admin panel
h. Improve docker and run apache or nginx and display site in browser and build interface for importing with uploading files





